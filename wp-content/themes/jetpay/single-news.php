<?php getHeader(array('color' => 'darkSecondary')); ?>
<div class="blog-archive container">
  <?php

while (have_posts()) {
  the_post(); ?>
  <div>
    <div class="spacer-4"></div>
    <h1 class="news-title underline">
      <?php the_title() ?>
    </h1>
    <p class="subtitle"><?php the_date(); ?></p>
    <div class="blog-main">
      <div class="blog-main__content">
        <?php the_content() ?>
        <div class="spacer-4"></div>
        <div class="spacer-4"></div>
      </div>
      <div class="blog-main__sidebar">
        <?php if(get_field('media_contact')) { ?>
        <h6 class="media-contact">
          Media Contact
        </h6>

        <?php get_field('media_contact'); ?>

        <?php } ?>
      </div>
    </div>

    <?php

} ?>

  </div>

  <?php
getFooter();