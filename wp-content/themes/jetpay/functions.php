<?php
// gets the composer files
require get_theme_file_path('vendor/autoload.php');
require get_theme_file_path('inc/includes.php');

// Clean up wordpres <head>
remove_action('wp_head', 'rsd_link'); // remove really simple discovery link
remove_action('wp_head', 'wp_generator'); // remove wordpress version
remove_action('wp_head', 'feed_links', 2); // remove rss feed links (make sure you add them in yourself if youre using feedblitz or an rss service)
remove_action('wp_head', 'feed_links_extra', 3); // removes all extra rss feed links
remove_action('wp_head', 'start_post_rel_link', 10, 0); // remove random post link
remove_action('wp_head', 'parent_post_rel_link', 10, 0); // remove parent post link
remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0); // remove the next and previous post links
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);

/**
 * Theme assets
 */
// Font Awesome CSS Pro
function hook_fa()
{ ?>
<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.5.0/css/all.css"
  integrity="sha384-j8y0ITrvFafF4EkV1mPW0BKm6dp3c+J9Fky22Man50Ofxo2wNe5pT1oZejDH9/Dt" crossorigin="anonymous" defer>
<?php

}
// register scripts and stylesheets
add_action('wp_head', 'hook_fa');

add_action('wp_enqueue_scripts', function () {
    $manifest = json_decode(file_get_contents('build/assets.json', true));
    $main = $manifest->app;
    wp_enqueue_style('theme-css', get_template_directory_uri() . "/build/" . $main->css, false, null);
    wp_enqueue_script('theme-js', get_template_directory_uri() . "/build/" . $main->js, ['jquery'], null, true);
}, 100);

// add admin styling to wordpress backend
add_action('admin_enqueue_scripts', function () {
    $manifest = json_decode(file_get_contents('build/assets.json', true));
    $main = $manifest->editor;
    wp_enqueue_style('admin-style', get_template_directory_uri() . "/build/" . $main->css, false, null);
    wp_enqueue_script('admin-js', get_template_directory_uri() . "/build/" . $main->js, null, null, true);
}, 100);

add_action('login_enqueue_scripts', function () {
    $manifest = json_decode(file_get_contents('build/assets.json', true));
    $main = $manifest->editor;
    wp_enqueue_style('custom_wp_admin_css', get_template_directory_uri() . "/build/" . $main->css, false, null);
    wp_enqueue_script('custom_wp_admin_js', get_template_directory_uri() . "/build/" . $main->js, null, null, true);

}, 100);


/**
 * Register our sidebars and widgetized areas.
 *
 */
function arphabet_widgets_init()
{

    register_sidebar(array(
        'name' => 'Footer Menu',
        'id' => 'footer-menu',
        'description' => 'This will be displayed as columns in the footer.',
        'before_widget' => '<div class="footer-column color--dark-secondary"><div class="footer-column-inner">',
        'after_widget' => '</div></div>',
        'before_title' => '<h5 class="footer-heading">',
        'after_title' => '</h5>',
    ));

}
add_action('widgets_init', 'arphabet_widgets_init');

/**
 * Theme setup
 */
add_action('after_setup_theme', function () {
    /**
     * Enable plugins to manage the document title
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#title-tag
     */
    add_theme_support('title-tag');
    /**
     * Register navigation menus
     * @link https://developer.wordpress.org/reference/functions/register_nav_menus/
     */
    register_nav_menus([
        'primary_navigation' => __('Primary Navigation', 'mini'),
        'footer_menu' => __('Footer Menu', 'mini')
    ]);
    /**
     * Enable post thumbnails
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support('post-thumbnails');
    /**
     * Enable HTML5 markup support
     * @link https://developer.wordpress.org/reference/functions/add_theme_support/#html5
     */
    add_theme_support('html5', ['caption', 'comment-form', 'comment-list', 'gallery', 'search-form']);
    /**
     * Enable selective refresh for widgets in customizer
     * @link https://developer.wordpress.org/themes/advanced-topics/customizer-api/#theme-support-in-sidebars
     */
    // add_theme_support('customize-selective-refresh-widgets');


}, 20);


add_action('rest_api_init', function () {
    $namespace = 'presspack/v1';
    register_rest_route($namespace, '/path/(?P<url>.*?)', array(
        'methods' => 'GET',
        'callback' => 'get_post_for_url',
    ));
});
/**
 * This fixes the wordpress rest-api so we can just lookup pages by their full
 * path (not just their name). This allows us to use React Router.
 *
 * @return WP_Error|WP_REST_Response
 */
function get_post_for_url($data)
{
    $postId = url_to_postid($data['url']);
    $postType = get_post_type($postId);
    $controller = new WP_REST_Posts_Controller($postType);
    $request = new WP_REST_Request('GET', "/wp/v2/{$postType}s/{$postId}");
    $request->set_url_params(array('id' => $postId));
    return $controller->get_item($request);
}
add_filter('body_class', 'add_slug_to_body_class'); // Add slug to body class (Starkers build)
// Add page slug to body class, love this - Credit: Starkers Wordpress Theme
function add_slug_to_body_class($classes)
{
    global $post;
    if (is_home()) {
        $key = array_search('blog', $classes);
        if ($key > -1) {
            unset($classes[$key]);
        }
    } elseif (is_page()) {
        $classes[] = sanitize_html_class($post->post_name);
        $classes[] = sanitize_html_class($post->post_type);
    } elseif (is_singular()) {
        $classes[] = sanitize_html_class($post->post_name);
        $classes[] = sanitize_html_class($post->post_type);
    } else {
        // this adds classname to custom post type archives
        $classes[] = sanitize_html_class($post->post_type);
    }
    return $classes;
}
// Redirect the Single Page Version of the 'Leadership' Post type

add_action('template_redirect', 'redirect_leadership_single');
function redirect_leadership_single()
{
    if (is_singular('leadership')) {
        wp_redirect(home_url(), 302);
        exit;
    }
}

// // prevent media posts from cluttering main namespace
// add_filter('wp_unique_post_slug', 'wpse17916_unique_post_slug', 10, 6);
// function wpse17916_unique_post_slug($slug, $post_ID, $post_status, $post_type, $post_parent, $original_slug)
// {
//     if ('attachment' == $post_type)
//         $slug = $original_slug . uniqid('-');
//     return $slug;
// }

// // prevent title widows
// add_filter( 'the_title', 'theme_widont' );

// Defer JS parsing
function defer_parsing_of_js($url)
{
    if (false === strpos($url, '.js')) return $url;
    if (strpos($url, 'jquery.js')) return $url;
    if (is_admin()) return $url;
    return "$url' defer ";
}
add_filter('clean_url', 'defer_parsing_of_js', 11, 1);


// adjust the default url-based query for wordpress page
function jetpayAdjustQueries($query)
{

   // use this query ONLY if it's not the admin AND it's the event post-archive page AND it's not a custom query.
    if (!is_admin() and is_post_type_archive('blog') and $query->is_main_query()) {
        $query->set('posts_per_page', 10);
    }

    // use this query ONLY if it's not the admin AND it's the event post-archive page AND it's not a custom query.
    if (!is_admin() and is_post_type_archive('news') and $query->is_main_query()) {
        $query->set('posts_per_page', 10);
    }

    // use this query ONLY if it's not the admin AND it's the event post-archive page AND it's not a custom query.
    if (!is_admin() and is_post_type_archive('announcement') and $query->is_main_query()) {
        $query->set('posts_per_page', 10);
    }
}



add_action('pre_get_posts', 'jetpayAdjustQueries');



/**
 * Extend WordPress search to include custom fields
 *
 * https://adambalee.com
 */

/**
 * Join posts and postmeta tables
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_join
 */
function cf_search_join($join)
{
    global $wpdb;

    if (is_search()) {
        $join .= ' LEFT JOIN ' . $wpdb->postmeta . ' ON ' . $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
    }

    return $join;
}

add_filter('posts_join', 'cf_search_join');

/**
 * Modify the search query with posts_where
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_where
 */
function cf_search_where($where)
{
    global $pagenow, $wpdb;

    if (is_search()) {
        $where = preg_replace(
            "/\(\s*" . $wpdb->posts . ".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
            "(" . $wpdb->posts . ".post_title LIKE $1) OR (" . $wpdb->postmeta . ".meta_value LIKE $1)",
            $where
        );
    }

    return $where;
}
add_filter('posts_where', 'cf_search_where');

/**
 * Prevent duplicates
 *
 * http://codex.wordpress.org/Plugin_API/Filter_Reference/posts_distinct
 */
function cf_search_distinct($where)
{
    global $wpdb;

    if (is_search()) {
        return "DISTINCT";
    }

    return $where;
}
add_filter('posts_distinct', 'cf_search_distinct');