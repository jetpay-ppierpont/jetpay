<?php getHeader(); ?>
<div>
  <?php

while (have_posts()) {
  the_post(); ?>
  <div v-pre class="page-builder">
    <?php the_content() ?>
  </div>

  <?php

} ?>

</div>

<?php
getFooter();