<?php
getHeader();
?>
  <div id="leadershipApp" class="leadership-archive">
    <div class="container">
      <div class="spacer-4"></div>
      <h1>Our leadership</h1>
      <div class="spacer-1"></div>
      <h3>Meet the leaders of JetPay.</h3>
      <hr/>
      <h4>Executive team:</h4>
      <div class="spacer-3"></div>
      <div class="leadership-archive__container">
      <?php
      while (have_posts()) {
        the_post();
        $name = get_field('exec_name');
        $title = get_field('exec_position');
        $photo = get_field('exec_headshot');
        $bio = get_field('exec_bio');
        ?>
        <leadership-modal
          name="<?php echo $name ?>"
          title="<?php echo $title ?>"
          img="<?php echo $photo ?>"
          bio="<?php echo $bio ?>"
          v-on:open-modal="showOverlay = true"
          v-on:close-modal="showOverlay = false"
        >
          <div class="leadership-card__photo"><img src="<?php echo $photo ?>"/></div>
          <div class="spacer-1"></div>
          <div class="leadership-card__name">
            <h6><?php echo $name ?></h6>
          </div>
          <div class="leadership-card__title">
            <p class="caption nomargin"><?php echo $title ?></p>
          </div>
        </leadership-modal>
        <?php

      } ?>
      </div>
    </div>
    <div class="spacer-4"></div>
<?php
getFooter();