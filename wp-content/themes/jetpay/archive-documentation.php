<?php getHeader(); ?>
<div class="documentation">
  <div class="documentation__header">
    <div class="documentation__header-title">
      <h1>The Knowledge base</h1>
      <docs-component></docs-component>
    </div>
  </div>
  <div class="documentation__body container">
    <div class="documentation__body-title">
      <div class="spacer-2"></div>
      <h2 class="relevant">Suggested:</h2>
      <div class="spacer-2"></div>
    </div>
    <div class="documentation__body-cards">
      <div class="category-posts helpful-links">
        <div class="spacer-1"></div>
        <h3 class="category-name">Popular Subjects</h3>
        <hr style="margin: .75em 0;">
        <div class="helpful-links_buttons">
          <?php $sidebar_categories = get_field('linked_categories','option');
      foreach($sidebar_categories as $category) {
        ?>
          <a href="<?php echo get_category_link($category); ?>" class="post-link">
            <p class="button-text"><?php echo get_cat_name($category); ?></p>
          </a>
          <?php
      }
      ?>
        </div>
      </div>
      <div class="category-posts helpful-links">
        <div class="spacer-1"></div>
        <h3 class="category-name">Popular Articles</h3>
        <hr style="margin: .75em 0;">
        <div class="helpful-links_buttons text">
          <?php $fav_posts = get_field('favorite_links','option');
      foreach($fav_posts as $post) {
        ?>
          <a href="<?php echo get_permalink($post); ?>" class="post-link">
            <p class="button-text"><?php echo get_the_title($post); ?></p>
          </a>
          <?php
      }
      ?>
        </div>
      </div>
      <?php
    $categories = get_field('categories_to_display', 'options');
    foreach ($categories as $category) {
      get_knowlegebase_category($category);
    }

    ?>
    </div>
  </div>
</div>


<?php
getFooter();