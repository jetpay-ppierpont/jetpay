<?php
getHeader(); ?>
<div class="category-header">
  <?php getCategoryHeader(); ?>
  <div class="container">
    <div class="spacer-2"></div>
    <a href="/docs" class="back-to-home"><i class="fas fa-arrow-left"></i>&nbsp;Knowledgebase home</a>
  </div>
</div>
<div class="category-main container">
  <?php getCategoryMain(); ?>
</div>


<?php
getFooter();