<?php
/*
 **
 * Template Name: Documentation - Page-Builder
 * Template Post Type: documentation
 **
 */
getHeader();
while (have_posts()) {
  the_post(); ?>
<div class="category-header">
  <?php getCategoryHeader(); ?>
  <div class="container">
    <div class="spacer-2"></div>
    <a href="/docs" class="back-to-home" style="width: fit-content;"><i class="fas fa-arrow-left"></i>&nbsp;Knowledge
      base home</a>
  </div>
</div>
<div id="page-builder">
  <?php the_content() ?>
</div>

<?php

} ?>
<?php
getFooter();