<?php getHeader(); ?>
<div class="blog-archive container">
  <div class="spacer-4"></div>
  <h1 class="changelog-header underline">Release Notes</h1>
  <div class="changelog-group">
    <?php
 $dateI;
  while (have_posts()) {
    the_post();
    $product = getTitle(get_field('product')['0']);
    $date = get_field('date');
    $rawDate = get_field('date');
    $currentDate = date("F Y", strtotime($rawDate));

    // headerDate($wp_query);

    if($dateI !== null) {
      if($currentDate !== $dateI) {
        echo "</div><div class='spacer-1'></div><h4 class='new-month-title color--dark-secondary'>" . $currentDate . "</h4>
        <div class='changelog-section'>";
      }
    } else {
      echo "<div class='spacer-1'></div><h4 class='new-month-title color--dark-secondary'>" . $currentDate . "</h4>
      <div class='changelog-section'>";
    }
    ?>
    <div class="changelog-container">
      <article class="changelog-entry">
        <a class="changelog normal-link" href="<?php the_permalink() ?>">
          <p class="caption"><?php echo date("m.d", strtotime($date)); ?></p>
          <p class="changelog-entry-title overline"><?php echo $product; ?></p>
          <p class="caption changelog-entry-summary"><?php echo get_field('brief_summary'); ?></p>

          <p class="view-changelog caption"><i class="changelog far fa-external-link"></i></p>
        </a>

      </article>
    </div>

    <?php
  $dateI = $currentDate;
} ?>
  </div>
</div>
</div>
<div class="spacer-1"></div>
<div class="link-pagination">
  <?php echo paginate_links(); ?>
</div>
<div class="spacer-2"></div>
<div class="spacer-4"></div>
<?php
getFooter();