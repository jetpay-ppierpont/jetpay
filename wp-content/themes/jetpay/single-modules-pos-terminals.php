<?php
getHeader();

?>
<div>
  <?php

while (have_posts()) {
  the_post();
  moduleHeader();
  ?>
  <div class="page-builder">
    <?php the_content() ?>
  </div>
  <?php
} ?>
  <div class="container">
    <?php getTerminals(); ?>
  </div>
</div>

<?php
getFooter();