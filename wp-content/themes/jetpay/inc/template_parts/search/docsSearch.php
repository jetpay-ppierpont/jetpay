<?php
/**
 * Register our custom route.
 */
function jetpay_docs_register_search_route() {
    register_rest_route('jetpay/v2', '/search_docs', [
        'methods' => WP_REST_Server::READABLE,
        'callback' => 'jetpay_docs_ajax_search',
        'args' => jetpay_docs_get_search_args()
    ]);
}
add_action( 'rest_api_init', 'jetpay_docs_register_search_route');

/**
 * Define the arguments our endpoint receives.
 */
function jetpay_docs_get_search_args() {
    $args = [];
    $args['s'] = [
       'description' => esc_html__( 'The search term.', 'jetpay' ),
       'type'        => 'string',
   ];

   return $args;
}

/**
 * Use the request data to find the posts we
 * are looking for and prepare them for use
 * on the front end.
 */
function jetpay_docs_ajax_search( $request ) {
    $posts = [];
    $results = [];
    // check for a search term
    if( isset($request['s'])) :
		// get posts
        $posts = get_posts([
            'posts_per_page' => 10,
            'post_type' => ['documentation'],
            's' => $request['s'],
        ]);

        function returnSomething($categories) {
          $lowestCategory = get_cat_name(array_reverse($categories)[0]);
          if ($lowestCategory == "") {
            $lowestCategory = "General";
          }
          return $lowestCategory;
        }
		// set up the data I want to return
        foreach($posts as $post):
            $results[] = [
                'title' => $post->post_title,
                'link' => get_permalink( $post->ID ),
                'type' => returnSomething($post->post_category),
                'excerpt'=>shortenExcerpt(strip_tags($post->post_content), 50)
            ];
        endforeach;
    endif;

    if( empty($results) ) :
        return new WP_Error( 'front_end_ajax_search', 'No results');
    endif;

    return rest_ensure_response( $results );
}