<?php
  // for the blog header

function getBlogHeader() { ?>
<div class="blog-header">
  <div class="blog-header_items">
    <a href="<?php echo get_post_type_archive_link('blog') ?>"
      class="<?php echo strpos($_SERVER['REQUEST_URI'], 'blog') ? "selected" : "" ?>">
      <p class="button-text">Blog</p>
    </a>
    <div class="button-text separator" style="padding-left: 0; padding-right: 0;">
      |
    </div>
    <a href="<?php echo get_post_type_archive_link('news') ?>"
      class="<?php echo strpos($_SERVER['REQUEST_URI'], 'news') ? "selected" : "" ?>">
      <p class="button-text">News</p>
  </div>
</div>
</a>
<?php }