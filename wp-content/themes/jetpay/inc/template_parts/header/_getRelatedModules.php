<?php



function getRelatedModules($productID)
{
  $getRelatedModules = new WP_Query(array(
    'posts_per_page' => -1, // how many posts we want per page (-1 is equal to "I want all of them", or we can explicitly state // how many we want )
    'post_type' => 'modules', // the type of post that we are querying
    'orderby' => 'title', // because it's numbers we use _num
    'order' => 'DESC', // what order we want the posts to go in...
    /*
     ** how we 'filter' the output.
     ** Formatted as 'nested' arrays that give us query parameters
     */
    'meta_query' => array(
      array(
        'key' => 'related_product', // if the array of related programs
        'compare' => 'LIKE', // contains the
        'value' => $productID // current post, then display the content. (needs to be formatted as "12")
      ),
    )
  ));

  // Collect all the titles and URLs in an array;

  ?>
<?php if ($getRelatedModules->have_posts()) {  ?>
<dropdown-component v-cloak trigger="<?php echo get_the_title($productID); ?> Modules">
  <?php } ?>

  <?php while ($getRelatedModules->have_posts()) {
    $getRelatedModules->the_post();
    ?>

  <a class="related-link" href="<?php the_permalink() ?>">
    <div class="link-content">
      <p class="related-title subheading"><?php (the_title()) ?></p>
      <p class="caption no-margin"><?php the_field('summary') ?></p>
      <hr>
    </div>
  </a>


  <?php

                            } ?>
</dropdown-component>

<?php wp_reset_postdata();
}