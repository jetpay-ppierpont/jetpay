<?php
 function getHeader($args = null)
 {
   ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head id="swup-head">
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
  <meta name="theme-color" content="#ff6600">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <?php if ($args['openGraph']) $args['openGraph']; ?>
  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
  <link rel="manifest" href="/manifest.json">

  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  <main id="vue-app">
    <header>
      <div class="container">

        <navigation-menu v-cloak></navigation-menu>

        <a id="logo" href="/">
          <img src="https://cdn2.hubspot.net/hubfs/2004225/Remastered_JetPayNCR.svg" />
        </a>

        <div class="header-menu right">
          <search-component v-cloak></search-component>

          <vue-modal v-cloak title="Login" action="Select Product"
            icon="<?php echo get_field('login_button_icon', 'options') ?>">


            <?php if (have_rows('product_logins', 'option')) :
     // loop through the rows of data
     while (have_rows('product_logins', 'option')) : the_row(); ?>
            <div>
              <div>
                <button style="font-size: 14px; text-transform: none;"
                  href="<?php the_sub_field('url_of_login_page'); ?>" class="filled large">
                  <?php the_sub_field('name_of_product'); ?>
                </button>
                <div class="spacer-1"></div>
              </div>
            </div>
            <?php endwhile;
     endif; ?>

          </vue-modal>
        </div>
      </div>


      </div>
    </header>
    <?php

     }