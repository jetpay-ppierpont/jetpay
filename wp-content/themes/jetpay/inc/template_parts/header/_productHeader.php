<?php

function productHeader($category, $theID, $home = null)
{

  $currentID = $theID;
  function currentClass($currentID)
  {
    return ($id == $currentID) ? 'current' : '';
  }

  $getProducts = new WP_Query(array(
    'posts_per_page' => 10,
    'post_type' => 'solutions', // the type of post that we are querying
    'meta_key' => 'category',
    'order' => 'ASC',
    'meta_query' => array(
      array(
        'key' => 'category',
        'compare' => '=',
        'value' => $category,
      ),
    )
  ));

  ?>


<div class="product-header">
  <div class="product-header__solution">
    <?php
      $inc = 1;
      while ($getProducts->have_posts()) : $getProducts->the_post(); ?>

    <a href="<?php the_permalink(); ?>" class="<?php echo $currentID == get_the_ID() ? "selected" : '' ?>">
      <p class="button-text"><?php htmlspecialchars_decode(the_title()); ?></p>
    </a>

    <?php if (((!$getProducts->current_post + 1) == ($getProducts->post_count)) and (($getProducts->post_count) !== (1))) {
        ?>
    <div class="button-text separator" style="padding-left: 2px; padding-right: 2px;">
      |
    </div>
    <?php

    }
    endwhile; ?>
  </div>
</div>

<div class="product-header-mobile">
  <select onchange="javascript:location.href = this.value;">
    <?php
      $inc = 1;
      while ($getProducts->have_posts()) : $getProducts->the_post(); ?>
    <option <?php if(get_the_ID() == $theID) {echo "selected";} ?> value="<?php the_permalink(); ?>">
      <p class="button-text"><?php htmlspecialchars_decode(the_title()); ?></p>
    </option>
    <?php
    endwhile; ?>
  </select>
</div>


<?php
        if ($home == null) {
          ?>

<div class="container">
  <div class="header-product">
    <a class="product-header-back button-text" href="/<?php echo $category; ?>">
      <?php echo '<i class="fal fa-long-arrow-left"></i>  ' . "&nbsp;" . "<span class='back-to-main-category'>" . htmlspecialchars_decode($category) . "</span>" ?>
    </a>
    <div class="product-header-related">
      <?php getRelatedModules($theID) ?>
    </div>
  </div>
</div>
<?php

    }
    ?>

<?php
  wp_reset_postdata();
}