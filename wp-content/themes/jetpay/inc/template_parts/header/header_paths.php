<?php

require __DIR__ . "/_blog-header.php";
require __DIR__ . "/_getRelatedModules.php";
require __DIR__ . "/_logo.php";
require __DIR__ . "/_moduleHeader.php";
require __DIR__ . "/_primary-navigation.php";
require __DIR__ . "/_productHeader.php";
require __DIR__ . "/_searchDropdown.php";
require __DIR__ . "/header.php";