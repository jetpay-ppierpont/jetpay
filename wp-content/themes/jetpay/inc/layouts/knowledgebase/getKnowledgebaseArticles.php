<?php

function get_knowlegebase_category($category) {
  $args = array(
    'posts_per_page' => 10,
    'post_type' => 'documentation', // the type of post that we are querying
    'cat' => "$category",

  );
  $kbQuery = new WP_Query($args);
  ?>

<div class="category-posts">
  <div class="spacer-1"></div>
  <h4 class="category-name"><?php echo get_cat_name($category); ?></h4>
  <hr style="margin: .75em 0;">
  <?php while ($kbQuery->have_posts()) : $kbQuery->the_post();?>

  <a href="<?php echo get_the_permalink(); ?>" class="post-link">
    <p class="paragraph"><?php the_title(); ?></p>
  </a>

  <?php endwhile;
  wp_reset_postdata(); ?>

</div>
<?php
}