<?php

function getCategoryHeader() {
?>
<div class="documentation">
  <div class="documentation__header small">
    <div class="documentation__header-title">
      <h5>The Knowledge base</h5>
      <docs-component></docs-component>
    </div>
  </div>
</div>
<?php
};