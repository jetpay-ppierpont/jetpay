<?php

function getCategoryMain() {
?>
<div class="spacer-4"></div>
<h1><?php single_cat_title(); ?></h1>
<div class="spacer-2"></div>
<?php
  while (have_posts()) {
    the_post();
    ?>
<a href="<?php the_permalink(); ?>">
  <div class="category-card">
    <div class="title">
      <h5><?php the_title(); ?></h5>
    </div>
    <hr style="margin: 1em 0em;">
    <div class="content">
      <?php echo excerpt(50); ?>
    </div>
    <div class="spacer-2"></div>
  </div>
</a>
<?php } ?>
<div class="spacer-1"></div>
<div class="link-pagination">
  <?php echo paginate_links(); ?>
</div>
<div class="spacer-2"></div>
<div class="spacer-4"></div>
<?php
}