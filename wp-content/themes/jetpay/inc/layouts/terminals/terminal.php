<?php

function getTerminals() {
  $getTerminals = new WP_Query(array(
    'posts_per_page' => -1,
    'post_type' =>'terminals',
    'order' => 'DEC', // what order we want the posts to go in...
  ));

  ?>
<div class="terminals-container" id="terminals">
  <?php

// Go through all terminals and create layout.
  while ($getTerminals->have_posts()) {
    $getTerminals->the_post(); ?>

  <div class="terminal-card">
    <a href="<?php the_permalink(); ?>" class="terminal-link">
      <div class="terminal-card__image" style="background-image:url('<?php echo get_field('image')?>')">
        <div class="terminal-card__header">
          <div class="terminal-card__header-title">
            <h3 style="text-align: center;"><?php echo get_field('brand'); ?></h3>
            <?php if (get_field('model_number')) {
            ?>
            <p class="subheading" style="text-align: center;"><?php echo get_field('model_number'); ?></p>
            <?php
          } ?>
          </div>
        </div>
      </div>
    </a>
  </div>

  <?php
  }
  ?>
</div>
<?php
}