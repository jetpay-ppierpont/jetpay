<?php

function listPartners($category, $properCategoryName)
{
  $args = array(
    'posts_per_page' => -1,
    'post_type' => 'partner', // the type of post that we are querying
    'meta_query' => array(
      array(
        'key' => 'category',
        'value' => $category,
        'compare' => 'LIKE',
      )
      ));
  $partnerQuery = new WP_Query($args);
  ?>
<div class="spacer-1"></div>
<h4 class="category-title" style="text-transform: capitalize;">
  <?php echo $properCategoryName ?> Partners
</h4>
<div class="partners-list" id="<?php $category?>">
  <?php
while ($partnerQuery->have_posts()) : $partnerQuery->the_post();?>
  <a href="<?php the_permalink();?>" class="link-to-partner-page">
    <div class="partner-item">
      <img src="<?php echo get_field('logo'); ?>" alt="<?php echo get_field('name') . " logo" ;?>" class="partner-logo">
      <div class="partner-overlay">
        <h6 class="overlay-title"><?php echo get_field('name') ?>&nbsp<i class="fas fa-long-arrow-right"></i></h6>
      </div>
    </div>
  </a>
  <?php endwhile; ?>
</div>
<?php
  wp_reset_postdata();
}