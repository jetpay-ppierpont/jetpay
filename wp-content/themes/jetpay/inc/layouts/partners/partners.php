<?php

require "_partnerCategory.php";

function getPartners()
{
  ?>
<div class="container">
  <div class="header">
    <div class="spacer-4"></div>
    <h1 class="underline">Partners</h1>
    <div class="retirement-partners">
      <!-- If you need to add a new partner name, just add a div, and call the function -->
      <!-- the first arg is the category value, the second just enter a more human-readable -->
      <!-- version of the category. e.g. ('value_add', "Value Added") -->
      <?php listPartners('retirement', 'Retirement'); ?>
    </div>
    <div class="payments-partners">
      <?php listPartners('payments', 'Payments'); ?>
    </div>
    <div class="value_add-partners">
      <?php listPartners('value_add', 'Value Added'); ?>
    </div>
  </div>
</div>
<?php
  wp_reset_postdata();
}