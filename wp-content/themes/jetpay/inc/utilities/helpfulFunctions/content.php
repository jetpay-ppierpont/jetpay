<?php


function content($limit, $content = null)
{
  $words = $content ? $content : unautop(get_the_content());
  $content = explode(' ', unautop(get_the_content()), $limit);
  if (count($content) >= $limit) {
    array_pop($content);
    $content = implode(" ", $content) . '...';
  } else {
    $content = implode(" ", $content);
  }
  $content = preg_replace('/[.+]/', '', $content);
  $content = apply_filters('the_content', $content);
  $content = wpautop(str_replace(']]>', ']]&gt;', $content));
  return $content;
}