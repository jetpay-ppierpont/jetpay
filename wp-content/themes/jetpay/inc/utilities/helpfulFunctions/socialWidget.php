<?php
function getSocialWidget()
{
  ?>
<div class="blog-main__sidebar-social">
  <h5>Social</h5>
  <p class="caption"><a
      href="<?php echo "https://www.facebook.com/sharer/sharer.php?u=https://www.facebook.com/sharer/sharer.php?u=http%3A" . get_the_permalink(); ?>">Facebook</a>
  </p>
  <p class="caption"><a
      href="<?php echo "https://twitter.com/home?status=http%3A" . get_the_permalink(); ?>">Twitter</a></p>
  <p class="caption"><a
      href="<?php echo "https://www.linkedin.com/shareArticle?mini=true&url=https%3A" . get_the_permalink() . "&title=" . urlencode(get_the_title()) . "&summary=" . get_field('social_media_summary') . "source=JetPay.com" ?>">Linkedin</a>
  </p>
</div>
<?php

}