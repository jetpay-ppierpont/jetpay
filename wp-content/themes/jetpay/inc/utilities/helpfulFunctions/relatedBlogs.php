<?php

function getRelatedBlogs($category, $product, $ID)
{
  $args = array(
    'posts_per_page' => 4,
    'post_type' => 'blog', // the type of post that we are querying
    'meta_query' => array(
      'relation' => 'or',
      array(
        'key' => 'related_product',
        'value' => $product,
        'compare' => 'LIKE',
      ),
      array(
        'key' => 'blog_category',
        'value' => $category,
        'compare' => 'LIKE',
      ),
    ),
  );
  $relatedCategoryBlogs = new WP_Query($args);
  ?>
<div class="blog-main__sidebar-related">
  <p class="sidebar-header"><strong>Related Blogs</strong></p>
  <?php
        $i = 0;
        while ($relatedCategoryBlogs->have_posts()) : $relatedCategoryBlogs->the_post();
        // the index ($i) is making sure we have 3 and we can skip posts that are the same index as the current post...
        if ($i < 3 && get_the_ID() != $ID) { ?>
  <p class="caption"><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></p>

  <?php $i++;
      }
      endwhile; ?>
</div>
<?php
  wp_reset_postdata();
}