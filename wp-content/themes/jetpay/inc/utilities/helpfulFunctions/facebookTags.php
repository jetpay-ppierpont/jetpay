<?php


function getFacebookTags($args = null)
{
  ?>
<meta property="og:url" content="<?php echo get_the_permalink() ?>" />
<meta property="og:type" content="article" />
<meta property="og:title" content="<?php echo get_the_title() ?>" />
<meta property="og:description" content="<?php echo get_field('social_media_summary') ?>" />
<meta property="og:image" content="<?php echo get_field('facebook_sharing_image') ?>" />
<?php

}