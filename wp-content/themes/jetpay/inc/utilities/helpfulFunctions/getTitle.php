<?php
function getTitle($args)
{
  $productPost = get_post($args);
  $title = isset($productPost->post_title) ? $productPost->post_title : '';
  return $title;
}