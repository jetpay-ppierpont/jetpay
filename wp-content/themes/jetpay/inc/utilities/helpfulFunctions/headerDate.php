<?php

// this function outputs the date of the changelog if it's different from the previous post.
function headerDate($wp_query)
{

  $prevPost = get_previous_post(true);
  $prevRawDate = get_field('date', $prevPost->ID);
  $rawDate = get_field('date');
  $currentDate = date("F Y", strtotime($rawDate));
  $prevDate = date("F Y", strtotime($prevRawDate));
  if ($wp_query->current_post === 0) {

    ?>
<h4 class="new-month-title color--dark-secondary"><?php echo $currentDate ?></h4>
<div class="changelog-section">
  <?php

} else {
  echo $currentDate;
  echo $prevDate;
  }
}