<?php
require __DIR__ . "/getTitle.php";
require __DIR__ . "/widont.php";
require __DIR__ . "/excerpt.php";
require __DIR__ . "/headerDate.php";
require __DIR__ . "/readMore.php";
require __DIR__ . "/content.php";
require __DIR__ . "/facebookTags.php";
require __DIR__ . "/removeDefaultPostTypes.php";
require __DIR__ . "/autoParagraph.php";
require __DIR__ . "/searchVars.php";
require __DIR__ . "/socialWidget.php";
require __DIR__ . "/relatedBlogs.php";