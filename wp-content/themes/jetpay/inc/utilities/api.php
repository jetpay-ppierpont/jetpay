<?php

// this get's the form fields from Hubspot so that we can use them in our front end.

use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;

add_action('rest_api_init', function () {
  register_rest_route('jetpay/v1', 'data', array(
    'methods' => 'GET', // the wp way of saying its a 'GET' request.
    'callback' => 'getFormFields'
  ));

});

function getFormFields($data)
{
  $client = new GuzzleHttp\Client();
  $res = $client->get('https://api.hubapi.com/forms/v2/fields/' . $data['form'] . '?hapikey=6c331119-b1ca-4e71-adc4-ae46b77ce31b');

  echo $res->getBody();
  exit(0);
}

// This let's us query all post types....

add_action('rest_post_query', 'getTypes', 10, 2);

function getTypes($args, $request)
{
  $post_types = $request->get_param('type');
  if (!empty($post_types)) {
    if (is_string($post_types)) {
      $post_types = array($post_types);
      foreach ($post_types as $i => $post_type) {
        $object = get_post_type_object($post_type);
        if (!$object || !$object->show_in_rest) {
          unset($post_types[$i]);
        }
      }
    }
    $args['post_type'] = $post_types;
  }
  return $args;
}