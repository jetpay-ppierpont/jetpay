<?php
/*
** IMPORT ALL THE FILES
*/
require __DIR__ . '/utilities/utilities.php';
require __DIR__ . '/plugins/plugins.php';
require __DIR__ . '/acf/acf.php';
require __DIR__ . '/template_parts/template_parts.php';
require __DIR__ . '/layouts/layouts.php';