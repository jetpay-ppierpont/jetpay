<?php
function getBlogPost($post, $index)
{
  // The BlogPost Template for the blog
  ?>
<div class="blog-post card-container" style="order: <?php echo $index ?>">
  <div class="blog-post card">
    <article class="blog-post__container">
      <a href="<?php echo $post['archive'] ?>">
        <h4 class="color--gray500 archive-link">Blog</h4>
      </a>
      <a class="normal-link" href="<?php echo $post['permalink']; ?>">
        <h3 class="card-title blog-post color--gray700">
          <?php echo $post['title']; ?>
        </h3>
      </a>
      <span class="meta-info__container">
        <p class="author-name"><?php echo $post['author'] ?></p>
        <p class="date caption spacing-medium-small"><?php echo $post['date'] ?></p>
      </span>

      <hr class="smooth spacing-medium">
      <span class="user-content">
        <?php echo wpautop($post['content']); ?>
      </span>
    </article>
  </div>
</div>
<?php

}