<?php
function fetch_data_for_blog_loop($paged)
{
  //Protect against arbitrary paged values
  // Query all of the post types...
  $getAllThings = new WP_Query(array(
    'posts_per_page' => 12,
    'paged' => $paged,
    'post_type' => array(
      'announcement',
      'blog',
      'changelog',
      'news',
    ), // the type of post that we are querying
    'order' => 'DEC', // what order we want the posts to go in...
  ));

  $allObjects = array();
// Go through and sort all of the post types into their respective arrays.
  while ($getAllThings->have_posts()) {
    $getAllThings->the_post();
// Get all things, and put them into an array (except for changelogs, they go in their own array).
    if (get_post_type() == "announcement") {
    // all data needed for Announcement Post
      $data = array(
        'post_type' => get_post_type(),
        'summary' => get_field('summary')['summary'],
        'color' => get_field('summary')['color'],
        'cta' => get_field('summary')['cta'],
        'permalink' => get_the_permalink(),
      );
      $date = get_the_date("U");
      $allObjects[$date][] = $data;
    }
    if (get_post_type() == "blog") {
    // all data need for Blog Post
      $data = array(
        'post_type' => get_post_type(),
        'date' => get_the_date(),
        'author' => get_field('author_name'),
        'permalink' => get_the_permalink(),
        'title' => get_the_title(),
        'archive' => get_post_type_archive_link('blog'),
        'content' => get_the_content(),
      );
      $date = get_the_date("U");
      $allObjects[$date][] = $data;
    }
    if (get_post_type() == "news") {
      // all data need for Blog Post
      $data = array(
        'post_type' => get_post_type(),
        'date' => get_the_date(),
        'author' => get_the_author_nickname(),
        'permalink' => get_the_permalink(),
        'excerpt' => content(60),
        'title' => get_the_title(),
        'archive' => get_post_type_archive_link('news'),
        'content' => get_the_content(),
      );
      $date = get_the_date("U");
      $allObjects[$date][] = $data;
    }
    if (get_post_type() == "changelog") {
    // all data needed for ChangeLog post.
      $data = array(
        'post_type' => get_post_type(),
        'author' => get_the_author_nickname(),
        "date" => get_the_date(),
        "archive" => get_post_type_archive_link('changelog'),
        "permalink" => get_the_permalink(),
        "product" => get_field('product')['0'],
        "releaseDate" => get_field('date'),
        "productTitle" => getTitle(get_field('product')['0']),
        'summary' => get_field('brief_summary'),
      );
    // get date of changelog release, change to timestamp
      $cLogDate = strtotime(get_field('date'));
    // change timestamp into string
      $theDate = date('F Y', $cLogDate);
    // change string into timestamp at first of the month
      $first_second_of_month = strtotime($theDate);
      $first_second_of_next_month = strtotime('+1 month', $first_second_of_month);
    // set the first moment of the beginning of the month equal to an array to store these changelogs within.
      $this_second = time();
      if ($first_second_of_next_month <= $this_second) {
        $allObjects[$first_second_of_next_month][] = $data;
      }
    }
  }
  $both = ['data' => $allObjects, 'query' => $getAllThings];
  return $both;
}