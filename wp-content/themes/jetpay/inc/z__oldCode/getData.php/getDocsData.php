<?php
/*
 **
 * This lets us get a single url for the top level documentation page of a product.
 **
 */
function getProductDocs($productID)
{
  $productDoc = false;

  $category = get_field_object('docs_menu', 'option')['value'];
  foreach ($category as $key => $value) {
    if ($value['related_product'][0] == $productID) {

      if ($docID = $value['overview_page'][0]) {
        $docID = $value['overview_page'][0];
        $permalink = get_the_permalink($docID);
        $productDoc = $permalink;
        return $productDoc;
      } else {
        $docID = $value['category_subsections'][0]['subheader_children'][0]['docs_page_link'];
        $permalink = get_the_permalink($docID);
        $productDoc = $permalink;
        return $productDoc;
      }

    }
  }
}