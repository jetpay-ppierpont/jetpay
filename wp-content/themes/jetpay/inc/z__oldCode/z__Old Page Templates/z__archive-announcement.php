<?php
jpHeader(array('color' => 'darkSecondary'));
?>

<div class="blog-archive container">
  <h1 class="blog-header__title color--dark-secondary">Announcements</h1>
  <div class="blog-archive__container grid">

    <?php
    while (have_posts()) {
      the_post();
      get_field('featured') ? $featured = 'featured' : $featured = null;
      ?>
      <div class="card rounded <?php echo $featured ?>">
        <a href="<?php the_permalink() ?>">
          <div class="blog-archive__header-image"
            style="background: linear-gradient(180deg, rgba(0, 0, 0, 0.3) 0%, rgba(255, 255, 255, 0) 39.56%), linear-gradient(180deg, rgba(0, 0, 0, 0) 24.73%, rgba(0, 0, 0, 0.1) 60.71%, rgba(0, 0, 0, 0.2) 74.36%, rgba(0, 0, 0, 0.4) 100%), url('<?php echo get_field('facebook_sharing_image'); ?>');"></div>
        </a>
        <div class="blog-preview__text-container">
          <a class="normal-link" href="<?php the_permalink() ?>">
            <h5><?php the_title(); ?></h5>
          </a>
          <?php if (get_field('featured')) {
            ?>
            <p><?php echo get_field('social_media_summary') ?></p>
            <?php

          }
          ?>
        </div>
      </div>

      <?php

    }
    ?>
  </div>
</div>
<?php
// Let's get the footer
jpFooter(array('color' => 'mono'));