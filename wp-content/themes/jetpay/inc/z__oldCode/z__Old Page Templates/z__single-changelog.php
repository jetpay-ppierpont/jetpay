<?php jpHeader(array('color' => 'darkSecondary')); ?>
<div class="blog-archive container">
<?php
while (have_posts()) {
  the_post();
  $product = get_field('product')['0'];
  $date = get_field('date');
  ?>
  <div>
    <a href="<?php echo get_post_type_archive_link('changelog') ?>">
    <p class="button"><i class="fas fa-arrow-alt-left"></i> All release notes</p>
    </a>
  </div>
  <br>
  <div class="blog-post card-container">
    <div class="blog-post card">
      <article class="blog-post__container" style="margin-bottom: 24px;">
        <h2>Release Notes</h2>
        <h6 class="card-title changelog" style="margin-bottom: 40px;"><?php echo date("Y.m.d", strtotime($date)) . " – " . getTitle($product); ?></h6>
        <?php
        // check if the flexible content field has rows of data
        if (have_rows('main_content_builder')) :

          // loop through the rows of data
        while (have_rows('main_content_builder')) : the_row();

        if (get_row_layout() == 'heading') :

        ?><h3 class="color--dark-secondary"><?php echo the_sub_field('heading_input') ?></h3>
        <?php

        elseif (get_row_layout() == 'subheading') :

        ?><h6><?php echo the_sub_field('subheading_input') ?></h6>
        <?php

        elseif (get_row_layout() == 'text') :

          echo the_sub_field('text_input');

        elseif (get_row_layout() == 'image') :

        ?><img style="max-width: 100%; margin-bottom: 1em" src="<?php echo the_sub_field('image_input') ?>">
        <?php


        endif;

        endwhile;

        else :

        // no layouts found

        endif;
        ?>
      </article>
    </div>
  </div>

<?php

} ?>

</div>

<?php
jpFooter(array('color' => 'color'));
