<?php
jpHeader(array('color' => 'darkSecondary'));
?>

<div class="blog-archive container">
  <h1 class="blog-header__title color--dark-secondary">Blog</h1>
  <div class="blog-archive__container grid">

    <?php
    $i = 1;
    while (have_posts()) {
      the_post();
      $featured = get_field('featured');
      ?>
      <div dataLink="<?php echo get_permalink(); ?>" class="card rounded <?php if ($featured) {
                                                                          echo 'featured featured-blog';
                                                                        } ?>">
        <div class="blog-preview__text-container">
        <?php if ($featured) { ?>
          <h6 class="color--gray500 featured-background-image" style="background: linear-gradient(180deg, rgba(0, 0, 0, 0.3) 0%, rgba(255, 255, 255, 0) 39.56%), linear-gradient(180deg, rgba(0, 0, 0, 0) 24.73%, rgba(0, 0, 0, 0.1) 60.71%, rgba(0, 0, 0, 0.1) 74.36%, rgba(0, 0, 0, 0.01) 100%), url('<?php echo get_field('facebook_sharing_image'); ?>')">
            Featured
          </h6>
          <a href="<?php the_permalink(); ?>">
            <h5>
              <?php the_title(); ?>
            </h5>
          </a>
          <p class="blog-archive-author-name"><?php echo get_field('author_name'); ?></p>
              <?php

            } else { ?>
            <a class="color--gray700" href="<?php the_permalink(); ?>">
              <h5 class="color--grey700"><?php the_title(); ?></h5>
            </a>
            <p class="blog-archive-author-name"><?php echo get_field('author_name'); ?></p>
                <?php

              } ?>
          <div class="excerpt">
            <?php if ($featured) {
              echo "<p>" . excerpt(100) . "</p>";
            } else {
              echo wpautop(content(25));
            }
            ?>
          <p class="caption no-margin"><?php echo get_the_date() ?></p>
          </div>
        </div>
      </div>

      <?php

    }
    ?>
  </div>

</div>
<div class="link-pagination">
  <?php echo paginate_links(); ?>
</div>

<?php
// Let's get the footer
jpFooter(array('color' => 'mono'));