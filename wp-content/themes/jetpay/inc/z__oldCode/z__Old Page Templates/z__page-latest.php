<?php jpHeader(array('color' => 'darkSecondary')); ?>
<div class="blog-archive container">
  <div class="latest-title__container">
    <h1 class="blog-header__title color--dark-secondary">The Latest</h1>
    <h3 class="news-blog-filter color--dark-secondary">
      <a href="/news" class="news">News</a>
      |
      <a href="/blog" class="blog">Blog</a>
    </h3>
  </div>
<?php
/*
 *  --- Data Query ---
 ** This wp query & logic is found in "{themeRootFile}/inc/utilities/getBlogData.php"
 **
 */
$paged = (get_query_var('paged')) ? absint(get_query_var('paged')) : 1;
$both = fetch_data_for_blog_loop($paged);
$data = $both['data'];
$big = 999999999; // need an unlikely integer
$query = $both['query'];

krsort($data);

/*
 **
 *  --- Create Blog Page ---
 **
 */

/* TO DO ---- MAKE NEWS PAGE SHOW UP THE SAME WAY THAT THE ANNOUNCEMENT PAGE DOES W/ SUMMARY */

// Index let's us place a signup form whereever we wish...
$index = 0;
foreach ($data as $postKey => $postObj) {

  $post = $postObj[0];
  $type = $postObj[0]['post_type'];

  // Announcement Post Type will be displayed as follows...
  if ($type == 'announcement') {
    getBlogAnnouncement($post, $index);
  }
  // Blog Post Type will be displayed as follows...
  if ($type == 'blog') {
    getBlogPost($post, $index);
  }
  // News Post Type will be displayed as follows...
  if ($type == 'news') {
    getNewsArticle($post, $index);
  }
  // Changelog Post Type will be displayed as follows...
  if ($type == 'changelog') {
    getBlogChangelog($postObj, $postKey, $index);
  }
  $index++;
}
getForm(array('title' => "Subscribe to get the latest updates!", "content" => "blog"));
?>
</div>
<?php
$pagination = paginate_links(array(
  'base' => str_replace($big, '%#%', esc_url(get_pagenum_link($big))),
  'format' => '?paged=%#%',
  'current' => max(1, get_query_var('paged')),
  'total' => $query->max_num_pages,
  'type' => 'list',
));
?>
<div class="pagination centered">
  <?php echo $pagination; ?>
</div>
<?php
wp_reset_postdata();
// Let's get the footer
jpFooter(array('color' => 'mono'));