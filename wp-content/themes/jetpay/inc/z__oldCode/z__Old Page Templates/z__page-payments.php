<?php jpHeader(array(
  'background-color' => '#f1f1f1',
  'color' => 'darkSecondary',
  'product_header' => array(
    'type' => 'payments',
    'location' => 'home'
  )
));
?>
<div style="top: 72px;" class="product-content">
<?php
while (have_posts()) {
  the_post();
  the_content();
}
?>
</div>
<?php
if (get_field('show_form')) getForm();



/*
 ** Notice -- custom footer function
 *	$args of jpFooter() can be color, mono,
 *	orange, teal, or blue.
 **
 */

jpFooter(array('color' => 'orange'));