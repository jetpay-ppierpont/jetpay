<?php
/*
Template Name: Product Page - Light Header
Template Post Type: solutions
 */

$ID = get_the_id(getUrl());

while (have_posts()) {
  the_post();
  $color = null;
  if (get_field('category') == 'payments') {
    $color = 'orange';
    $home = array(
      'link' => '/payments',
      'title' => 'Payments'
    );
  } else {
    $color = 'teal';
    $home = array(
      'link' => '/payroll',
      'title' => 'Payroll'
    );
  }
  jpHeader(array(
    'color' => 'lightPrimary',
    'linkColor' => '#fff'
  ));
  // productHeader(get_field('category'));
  ?>
  <div style="position: absolute; width: 100%; background: transparent; top: 80px; z-index: 100;">
    <div style="
    position: relative;
    padding: 16px;
    max-width: 906px;
    margin: auto;
    ">
      <div class="product-header__links-left" >
        <a href="<?php echo $home['link'] ?>">
          <p class="no-margin color--light-primary" style="opacity: .78;"><i class="fal fa-long-arrow-left"></i>  <?php echo $home['title'] ?></p>
        </a>
      </div>

      <div class="product-header__links-right" >
        <a href="<?php echo getProductDocs($ID); ?>">
          <p class="no-margin color--light-primary" style="opacity: .78;">Docs</p>
        </a>
      </div>

      <div class="product-header__links-right" >
          <a class="collapsible" href="#0">
          <p class=" no-margin color--light-primary" style="opacity: .78;">Modules</p>
        </a>
        <?php getRelatedModules($ID) ?>
      </div>


      <div class="product-header__links-right" >
        <a href="#">
          <p class="no-margin color--light-primary">Overview</p>
        </a>
      </div>

    </div>
  </div>
  <div class="product-content" style="position: relative; top: -60px;">
  <?php
  the_content();
  ?>
  <?php
  if (get_field('show_form')) getForm();



  /*
   ** Notice -- custom footer function
   *	$args of jpFooter() can be color, mono,
   *	orange, teal, or blue.
   **
   */

  jpFooter(array('color' => $color));
}
