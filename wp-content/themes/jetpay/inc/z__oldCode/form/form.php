<?php

function getForm($args = null, $display = null)
{

  $args['show'] ? $args['show'] : false;
  $args['title'] ? $args['title'] : "Sign Up";
  $args['column'] ? $args['column'] : "column";
  $args['color'] ? $args['color'] : "orange";
  $args['formId'] ? $args['formId'] : "2cef4e56-71ea-4eca-a5ce-d3ebb646ea42";
  $args['content'] == 'blog' ? $args['content'] = "Did something here interest you? Subscribe now to get more articles, just like this one." : null;



  $showForm = get_field('show_form') ? get_field('show_form') : $args['show'];
  $formTitle = get_field('form_title') ? get_field('form_title') : $args['title'];
  $formColumn = get_field("column") ? get_field("column") : $args['column'];
  $buttonColor = strtolower(get_field('button_color')) ? strtolower(get_field('button_color')) : $args['color'];
  $formId = get_field('hubspot_form') ? get_field('hubspot_form') : $args['formId'];

  if (!$display) {
    ?>
  <!-- JavaScript Logic to determine wether to run the forms -->
<div id="signup-form" v-cloak style="order: 0;">
  <transition name="fade">
    <div v-if="isLoaded" class="container align-center form vertical-spacing-large">
      <?php if (!$args['content']) {
        ?>
        <h5 class="form-title has-text-centered"><?php echo $formTitle ?></h5>
        <?php

      } ?>

      <?php if ($args['content']) {
        ?>
        <div class=form-content__container>
          <h5 class="form-title has-text-centered"><?php echo $formTitle ?></h5>
          <p><?php echo $args['content'] ?></p>
        </div>
        <?php

      } ?>
      <div class="form-field__container <?php echo $formColumn ?>">

    <!-- our signup form ===================== -->


        <form id="hubspot-form"  v-on:submit.prevent="submit" >
          <!-- name -->
          <div v-for="formField in formFields" class="field">
            <label class="label">{{formField.label}}</label>
            <input v-bind:type="formField.type" v-bind:required="formField.required" class="input" v-model.trim="formField.value" v-bind:placeholder="formField.placeholder" >
          </div>
          <div>
          <transition name="fade">
            <p class="caption color--green500" style="margin-top: 0px" v-if="showSuccessMessage">{{successResponse}}</p>
            <p class="caption color--red400" style="margin-top: 0px" v-if="showFailMessage">{{failResponse}}</p>
          </transition>
          </div>
          <!-- submit button -->
          <div class="button-container">
            <button class="button <?php echo $buttonColor ?>">Submit</button>
          </div>
        </form>
      </div>
    </div>
  </transition>

</div>


  <?php

}

}