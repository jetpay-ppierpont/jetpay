<?php

function productHeader($category, $home = null)
{
  $currentID = get_the_ID();
  function currentClass($id)
  {
    return ($id == $currentID) ? 'current' : '';
  }

  $data = null;
  if ($category == 'payroll') {
    $data['color'] = 'teal800';
  }
  if ($category == 'payments') {
    $data['color'] = 'orange700';
  }

  $getProducts = new WP_Query(array(
    'posts_per_page' => 10,
    'post_type' => 'solutions', // the type of post that we are querying
    'meta_key' => 'category',
    'order' => 'ASC',
    'meta_query' => array(
      array(
        'key' => 'category',
        'compare' => '=',
        'value' => $category,
      ),
    )
  ));
  ?>
  <div class="product-heading-spacer" style="height: 60px;"></div>
  <div style="position: relative; width: 100vw; margin-bottom: 26px;" class="background--<?php echo $data['color']; ?> desktop-size">
    <div class="product_header-container" style="max-width: 906px; width: 100%; margin: auto; display: flex; justify-content: center; ">
    <?php
    if ($home == null) {
      ?>
      <a  class="product-header_category-home" style="position: absolute; left: 0;" href="/<?php echo $category; ?>">
        <div style="padding: 8px 24px; margin: 8px 0;">
          <p class="button no-margin color--gray50"><?php echo '<i class="fal fa-long-arrow-left"></i>  ' . $category ?></p>
        </div>
      </a>
      <?php

    }
    ?>

      <?php
      $inc = 1;
      while ($getProducts->have_posts()) : $getProducts->the_post(); ?>
        <a href="<?php the_permalink(); ?>" style="<?php echo $currentID == get_the_ID() ? 'opacity: .7;' : '' ?>">
          <div style="padding: 8px 24px; margin: 8px 0;">
            <p class="button no-margin color--gray50"><?php the_title(); ?></p>
          </div>
        </a>
        <?php if (((!$getProducts->current_post + 1) == ($getProducts->post_count)) and (($getProducts->post_count) !== (1))) {
          ?>
          <div class="button no-margin" style="color: rgba(255,255,255, .5); display: flex; align-items: center; font-size: 24px; padding-bottom: 4px;">
          |
        </div>
          <?php

        }
        endwhile;
        ?>
    </div>
  </div>


  <?php
  wp_reset_postdata();
}
