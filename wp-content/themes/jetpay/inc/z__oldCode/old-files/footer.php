<?php

function jpFooter($args = null)
{

  if (!$args['color']) {
    $args['color'] = "color";
  }
  if (!$args['form']) {
    $args['form'] = "2cef4e56-71ea-4eca-a5ce-d3ebb646ea42";
  }
  // possible options are:
  // - color
  // - orange
  // - blue
  // - teal
	// - grey
  $color = $args['color'];
  $colorUrl = "images/$color-box.svg";

  ?>
	</main>
  <!-- </div> -->
	<footer>
		<div class="footer-top"></div>
		<div class="footer">
			<!-- <div class="footer-menu--container"> -->
				<div class="footer-menu">
					<?php dynamic_sidebar('footer-menu') ?>
				</div>
				<div id="swup-footer-image" class="footer-cubes footer-image--<?php echo $args['color'] ?>"
				style="background-image: url('<?php echo get_theme_file_uri($colorUrl); ?>')"></div>
				<div class="footer-bottom--section">
					<?php Logo(array('size' => '130', 'color' => 'darkDisabled')); ?>
					<p class="overline color--dark-secondary">© <?php echo date("Y"); ?> JetPay Corporation</p>
				</div>
			<!-- </div> -->
		</div>
	</footer>
		</div>
		<script>
		var wpFormEntryResponse = "<?php if (get_field('success_message')) {
                              echo get_field('success_message');
                            } else {
                              echo "Thank you for submitting this form!";
                            } ?>";
		var wpPostType = "<?php echo get_post_type(); ?>";
		var wpPageTitle = "<?php echo get_the_title(); ?>";
		var wpId = "<?php echo get_the_ID(); ?>";
		var wpSite = "<?php echo get_site_url() ?>";
		// show form?
		var wpFormId = "<?php
		// If the form is "custom"
                  if (get_field('hubspot_form') == "custom") {
			// If the custom form is defined, use that
                    if (get_field('hubspot_form_id')) {
                      echo get_field('hubspot_form_id');
                    } else {
				// if the custom form is not defined, use the default
                      echo $args['form'];
                    };
			// if the form is not custom...
                  } else {
			// if the form is defined
                    if (get_field('hubspot_form')) {
				// use the custom form
                      echo get_field('hubspot_form');
                    } else {
				// otherwise, use the default form..
                      echo $args['form'];
                    }
                  }

                  ?>"
		</script>
		<span id="swup-footer">
			<?php wp_footer(); ?>
		</span>
	</body>
</html>
  <?php

}