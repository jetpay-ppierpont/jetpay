
function jpHeader($args = null)
{

  $background_color = null;
  /*
   * Defining Default Variables
   ** e.g. (color, logosize, linkColor)
   */

  // Logo Color
  if (!$args['color']) {
    $args['color'] = 'color';
  }
  // Body Color
  if (!$args['bodyColor']) {
    $args['bodyColor'] = '';
  }
  // Logo Size
  if (!$args['logoSize']) {
    $args['logoSize'] = '150';
  }
  // Link Color
  if (!$args['linkColor']) {
    if ($args['color'] == 'color') {
      $args['linkColor'] = 'darkSecondary';
      $args['navTrigger'] = 'color-dark';
    } else {
      $args['linkColor'] = $args['color'];
      $args['navTrigger'] = 'color-white';
    }
  }


  /*
   * Now we output our HTML
   */
  ?>
  <!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head id="swup-head">
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
    <meta name="theme-color" content="#ff6600">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php if ($args['openGraph']) $args['openGraph']; ?>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <link rel="manifest" href="/manifest.json">
    <script>

    </script>
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?> >
		<div class="content-wrapper">
			<div class=content>
            <a class="swup-logo" href="<?php echo site_url() ?>">
            <?php Logo(array('size' => '130', 'color' => $args['color'])); ?>
            </a>

        <?php
        jetpay_navigation($args);
        if ($args['product_header']) {
          productHeader($args['product_header']['type'], $args['product_header']['location']);
        }
        ?>

        <div id="modal-bg"></div>
        <main id="swup" >
        <?php

      }


      function jetpay_navigation($args)
      {
        if ($args['background-color']) {
          $background_color = 'style="background-color:' . $args['background-color'] . ';"';
        }
        $main_menu = get_field('main_menu', 'option');
        ?>
    <div style="height: 80px;" class="product-heading-spacer"></div>
    <header <?php echo $background_color; ?>
    class="cd-morph-dropdown">
      <a href="#0" class="nav-trigger <?php echo $args['color']; ?>" >Open Nav<span aria-hidden="true"></span></a>
      <nav class="main-nav">
        <ul>
          <li class="has-dropdown links" tabindex="1" data-content="jp-menu-1">
            <a class="paragraph no-margin" style="color:<?php echo $args['linkColor']; ?>" href="#0"><?php echo $main_menu['slot_1']['title'] ?></a>
          </li>

          <li class="has-dropdown links" tabindex="1" data-content="jp-menu-2">
            <a class="paragraph no-margin" style="color:<?php echo $args['linkColor']; ?>" href="#1"><?php echo $main_menu['slot_2']['title'] ?></a>
          </li>

          <li class="has-dropdown links" tabindex="1" data-content="jp-menu-3">
            <a class="paragraph no-margin" style="color:<?php echo $args['linkColor']; ?>" href="#2"><?php echo $main_menu['slot_3']['title'] ?></a>
          </li>


            <?php getSearchDropdown($args); ?>


          <li data-content="support">
            <a class="paragraph no-margin" tabindex="1" style="color:<?php echo $args['linkColor']; ?>" href="<?php echo $main_menu['fixed_link_2']['link'] ?>"><?php echo $main_menu['fixed_link_2']['title'] . "<span style='margin: 0 4px;'></span>" . $main_menu['fixed_link_2']['icon'] ?></a>
          </li>
        </ul>
      </nav>

      <div class="morph-dropdown-wrapper">
        <div class="dropdown-list">
          <ul style="margin-top: 0;">
            <!-- This is the Solutions Section -->
            <li id="jp-menu-1" class="dropdown links">
              <a href="#0" class="label"><?php echo $main_menu['slot_1']['title'] ?></a>
              <div class="content">
                  <?php
                  largeIconHandler($main_menu['slot_1']['large_icons']);
                  ?>
                <div style="margin: 16px;"></div>
                <?php
                smallIconHandler($main_menu['slot_1']['small_icons']);
                customIconHandler($main_menu['slot_1']['small_icons_custom']);
                ?>
            </li>

            <li id="jp-menu-2" class="dropdown links">
              <a href="#1" class="label"><?php echo $main_menu['slot_2']['title'] ?></a>
              <div class="content">
                  <?php
                  largeIconHandler($main_menu['slot_2']['large_icons']);
                  ?>
                <div style="margin: 16px;"></div>
                <?php
                smallIconHandler($main_menu['slot_2']['small_icons']);
                customIconHandler($main_menu['slot_2']['small_icons_custom']);
                ?>
            </li>

            <li id="jp-menu-3" class="dropdown links">
              <a href="#2" class="label"><?php echo $main_menu['slot_3']['title'] ?></a>
              <div class="content">
                  <?php
                  largeIconHandler($main_menu['slot_3']['large_icons']);
                  ?>
                <div style="margin: 16px;"></div>
                <?php
                smallIconHandler($main_menu['slot_3']['small_icons']);
                customIconHandler($main_menu['slot_3']['small_icons_custom']);
                ?>
            </li>
          </ul>

          <div class="bg-layer" aria-hidden="true"></div>
        </div> <!-- dropdown-list -->
      </div> <!-- morph-dropdown-wrapper -->
  </header>
  <?php

}


function smallIconHandler($menu_items = null)
{
  if ($menu_items) {
    foreach ($menu_items as $key => $a) {
      iconNavFeature($a['Icon'], $a['background'], $a['background_shade'], $a['color'], $a['color_shade'], $a['title'], $a['subtitle'], $a['link']);
    }
    ?><div style="padding-bottom: 16px;"></div><?php

                                            } else {
                                              return null;
                                            }
                                          }


                                          function iconNavFeature($classes = "fas fa-building", $bg = "green400", $bgShade = "50", $color = "null", $colorShade = "50", $title = "title", $subtitle = "need subtitle", $link = "/")
                                          {
                                            ?>
  <div class="content-small">
    <div class="nav-list__item-container" style="width: auto; margin: 0 auto; padding: 0;">
      <a class="nav-solutions__item-link" style="margin: 40px 0" href="<?php echo $link; ?>">
        <div class="item-link-container">
          <div class="item-link-icon-container">
            <div class="nav-list__item-icon item-link-icon background--<?php echo $bg . $bgShade ?>" style="position: relative;">
              <i class="<?php echo "fal " . $classes . " " . "color--" . $color . $colorShade; ?>" style="position: absolute; position: absolute;  width: 100%; text-align: center; height: 100%; top: 24%;"></i>
            </div>
            </div>
          <div class="item-link__text-container">
            <span class="paragraph text-thick item-link-small-title color--gray700"><?php echo $title; ?></span>

            <span class="item-link-small-subtitle"><?php echo $subtitle; ?></span>
          </div>
        </div>
      </a>
    </div>
  <?php

}

function customIconHandler($menu_items = null)
{

  if ($menu_items) {
    ?><div style="padding-top: 16px; background: #f5f5f5;"></div><?php
                                                                foreach ($menu_items as $key => $a) {
                                                                  smallNavFeature($a['image'], $a['color'], null, $a['title'], $a['subtitle'], $a['link']);
                                                                }
                                                              } else {
                                                                return null;
                                                              }
                                                            }


                                                            function smallNavFeature($image = null, $color = "orange", $bgClass = null, $title = "title", $subtitle = "need subtitle", $link = "/")
                                                            {
                                                              ?>
  <div class="content-small dark">
    <div class="nav-list__item-container" style="width: auto; margin: 0 auto; padding: 0px 0 0;">
      <a class="nav-solutions__item-link" style="margin: 40px 0" href="<?php echo $link; ?>">
        <div class="item-link-container">
          <div class="item-link-icon-container">
            <div class="nav-list__item-icon item-link-icon" style="background-image: url('<?php echo $image; ?>');"></div>
            </div>
          <div class="item-link__text-container">
            <span class="button text-thick item-link-small-title color--<?php echo $color; ?>400"><?php echo $title; ?></span>

            <span class="item-link-small-subtitle"><?php echo $subtitle ?></span>
          </div>
        </div>
      </a>
    </div>
  <?php

}

function largeIconHandler($menu_items = null)
{
  if ($menu_items) {
    ?>
    <div class="nav-list__item-container" style="width: auto; margin: 0 auto; padding: 16px 0 0;">
<?php
foreach ($menu_items as $key => $menu_item) {
  ?>
  <a class="nav-solutions__item-link" style="margin: 40px 0" href="<?php echo $menu_item['link']; ?>">
    <div class="item-link-container">
      <div class="item-link-icon-container">
      <div class="nav-list__item-icon item-link-icon" style="background-image: url('<?php echo $menu_item['image']; ?>');"></div>
      </div>
      <div class="nav-solutions__item-container">
        <p class="color--gray700 subheading"><?php echo $menu_item['title']; ?></p>
        <p class="color--gray700 paragraph no-margin"><?php echo $menu_item['subtitle']; ?></p>
      </div>
    </div>
  </a>
  <?php

}
?>
  </div>
<?php

} else {
  return null;
}
}

function getNews()
{
  $getNews = new WP_Query(array(
    'posts_per_page' => 1,
    'post_type' => 'news', // the type of post that we are querying
    'order' => 'ASC',
  ));

  while ($getNews->have_posts()) : $getNews->the_post(); ?>
<div class="homepage__new-post--container">
  <div class='homepage__new-post'>
    <div class="homepage__new-post-container">
      <div class="homepage__new-post-new">
        <p class="caption">NEW</p>
      </div>
      <div class="homepage__new-post-title">
        <p class="caption"><?php the_title() ?></p>
      </div>
    </div>
  </div>
</div>
<?php endwhile;
}

function jpnHeader($args = null)
{
  ?>
  <!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head id="swup-head">
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
    <meta name="theme-color" content="#ff6600">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <?php if ($args['openGraph']) $args['openGraph']; ?>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <link rel="manifest" href="/manifest.json">

		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?> >
    <header>
        <div class="container">
          <div class="header-menu left"><a id="menu-button" href="#1"><b>Menu</b></a></div><a id="logo" href="#"><img src="https://cdn2.hubspot.net/hubfs/2004225/Remastered_JetPayNCR.svg"/></a>
          <div class="header-menu right"><b id="search-button">Search</b><b class="login-button large">Login</b></div>
        </div>
      </header>
      <div class="navigation-panel closed">
        <div class="navigation-panel__container">
          <div class="navigation-panel__container-normal"><a class="follow-link" href="/boomerang/iFrameKey-92e70743-ed99-35a1-0573-a376cd0ea451/index.html">
              <p class="button-text">Home</p></a><a class="follow-link" href="#">
              <p class="button-text" data="about">About</p></a><a class="follow-link login-button small" href="/boomerang/iFrameKey-92e70743-ed99-35a1-0573-a376cd0ea451/index.html">
              <p class="button-text">Login</p></a></div>
          <div class="navigation-panel__container-deep" parent="about"><a class="reset-list">
              <p>- Back</p></a><a class="follow-link" href="#">
              <p class="button-text">About Us</p></a><a class="follow-link" href="#">
              <p class="button-text">Contact</p></a></div>
        </div>
      </div>
        <?php

      }
