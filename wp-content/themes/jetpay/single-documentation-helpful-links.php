<?php
getHeader();
while (have_posts()) {
  the_post(); ?>
<div class="category-header">
  <?php getCategoryHeader(); ?>
  <div class="container">
    <div class="spacer-2"></div>
    <a href="/docs" class="back-to-home" style="width: fit-content;"><i class="fas fa-arrow-left"></i>&nbsp;Knowledge
      base home</a>
  </div>
</div>
<div id="normal-content" class="container">
  <div class="title">
    <div class="spacer-3"></div>
    <h1 class="heading underline"><?php the_title(); ?></h1>
    <div class="spacer-1"></div>
    <p class="subtitle">Helpful Links for HR professionals.
    </p>
    <div class="items-container">
      <?php
      // begin repeater field
      //check if the field has data
      if( have_rows('forms') ):
        while ( have_rows('forms') ) : the_row();?>

      <?php if(get_sub_field('form_or_link') === 'url') {
          ?>
      <a href="<?php the_sub_field('link'); ?>" class="icon-box-link">
        <div class="icon-box">
          <span class="icon-holder">
            <?php the_sub_field('icon'); ?>
          </span>
          <h5 class="icon-title"><?php the_sub_field('title') ?></h5>
          <p><?php the_sub_field('description'); ?></p>
        </div>
      </a>
      <?php
        } ?>
      <?php if(get_sub_field('form_or_link') === 'pdf') {
          ?>
      <a href="<?php the_sub_field('a_form'); ?>" class="icon-box-link">
        <div class="icon-box">
          <span class="icon-holder">
            <?php the_sub_field('icon'); ?>
          </span>
          <h5 class="icon-title"><?php the_sub_field('title') ?></h5>
          <p><?php the_sub_field('description'); ?></p>
        </div>
      </a>
      <?php
        } ?>


      <?php endwhile;

    else :

      // no rows found

    endif;
      ?>
    </div>
  </div>
  <?php the_content() ?>
  <div class="spacer-4"></div>
</div>

<?php

} ?>
<?php
getFooter();