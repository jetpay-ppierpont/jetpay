<?php
getHeader();
productHeader("payments", null, "home");
?>
<div>
  <?php

while (have_posts()) {
  the_post(); ?>
  <div v-pre class="page-builder">
    <?php the_content() ?>
  </div>

  <?php

} ?>

</div>

<?php
getFooter();