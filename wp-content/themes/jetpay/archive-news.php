<?php
getHeader(array('color' => 'darkSecondary'));
getBlogHeader();
?>

<div>
  <div class="container">
    <div class="spacer-4"></div>
    <h1>News</h1>
    <div class="spacer-1"></div>
    <h3>The latest information.</h3>
    <hr />
    <h4>Recent stories:</h4>
    <div class="spacer-1"></div>
    <div class="news-archive__container">
      <?php while (have_posts()) {
  the_post();
  $title = get_the_title();
  $content = excerpt(25);
  $date = get_the_date();
  ?>
      <div linkto="<?php the_permalink(); ?>" class="card">
        <p class="overline spaced-natural"><?php echo $date ?></p>
        <h3 class="underline"><?php echo $title ?></h3>
        <p class="paragraph"><?php echo $content ?></p>
        <br>
      </div>
      <?php

} ?>

      <div class="spacer-1"></div>
      <div class="link-pagination">
        <?php echo paginate_links(); ?>
      </div>
      <div class="spacer-2"></div>
    </div>
  </div>
</div>
<?php
getFooter();