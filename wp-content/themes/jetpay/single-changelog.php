<?php
getHeader();
?>

<div class="container">
  <div class="spacer-2"></div>
  <p class="overline">Changelog</p>
  <h1 style="font-family: 'Open Sans'"><?php the_title(); ?></h1>
  <div class="spacer-2"></div>
  <?php
        // check if the flexible content field has rows of data
        if (have_rows('main_content_builder')) :

          // loop through the rows of data
        while (have_rows('main_content_builder')) : the_row();

        if (get_row_layout() == 'heading') :

        ?>
  <div class="spacer-2"></div>
  <h3 style="font-family: 'Open Sans'" class="color--dark-secondary"><?php echo the_sub_field('heading_input') ?></h3>
  <?php

        elseif (get_row_layout() == 'subheading') :

        ?>
  <div class="spacer-1"></div>
  <h6>
    <?php echo the_sub_field('subheading_input') ?></h6>
  <?php

        elseif (get_row_layout() == 'text') :

          echo the_sub_field('text_input');

        elseif (get_row_layout() == 'image') :

        ?><img
    style="max-width: 100%; margin-bottom: 1em" src="<?php echo the_sub_field('image_input') ?>">
  <?php


        endif;

        endwhile;

        else :

        // no layouts found

        endif;
        ?>
</div>
<div class="spacer-4"></div>
<div class="spacer-4"></div>
<?php
getFooter();
?>