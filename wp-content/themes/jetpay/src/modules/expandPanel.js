export default function expandPanel() {
  var coll = document.getElementsByClassName("collapsible");
  var i;

  for (i = 0; i < coll.length; i++) {
    var el = coll[i];
    var content = el.nextElementSibling;
    if (el.classList.contains("active")) {
      content.style.maxHeight = content.scrollHeight + "px";
    } else {
      content.style.maxHeight = null;
    }

    el.addEventListener("click", function() {
      this.classList.toggle("active");
      var cont = this.nextElementSibling;
      if (this.classList.contains("active")) {
        cont.style.maxHeight = content.scrollHeight + "px";
      } else {
        cont.style.maxHeight = null;
      }
    });
  }
}
