export default {
  init() {
    // JavaScript to be fired on all pages
    console.log("blog");
  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
    const cards = [...document.getElementsByClassName("card")];
    console.log(cards);
    cards.map(card => {
      const url = card.attributes.dataLink.value;
      card.addEventListener("click", () => {
        window.location = `${url}`;
      });
    });
  }
};
