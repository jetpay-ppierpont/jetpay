import jQuery from 'jquery';
import Vue from 'vue';
import ComponentList from './components/ComponentList';
import './_style.scss';
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import leadership from './routes/leadership';
import taxLinksByState from './routes/taxLinksByState';
import './images/white-texture.svg';
/**
 * Populate Router instance with DOM routes
 * @type {Router} routes - An instance of our router
 */

const routes = new Router({
	/** All pages */
	common,
	/** Home page */
	home,
	/* Leadership Archive Page */
	leadership,
	// Tax links by state map
	taxLinksByState
});

/** Load Events */
jQuery(document).ready(() => routes.loadEvents());
global.vue = new Vue({
	el: '#vue-app',
	components: {
		...ComponentList
	},
	data: {
		showOverlay: false
	}
});
