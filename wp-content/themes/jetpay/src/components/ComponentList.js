import LeadershipModal from './modal/leadership-modal.vue';
import NavigationMenu from './menu/navigation-menu.vue';
import NavItems from './menu/nav-items.vue';
import SearchComponent from './search/search.vue';
import DocsComponent from './search/docs.vue';
import LoginComponent from './login/login.vue';
import DropdownComponent from './dropdown/dropdown-component.vue';
import VueModal from './modal/standard/modal.vue';
export default {
	VueModal,
	LeadershipModal,
	NavigationMenu,
	NavItems,
	SearchComponent,
	DocsComponent,
	LoginComponent,
	DropdownComponent
};
