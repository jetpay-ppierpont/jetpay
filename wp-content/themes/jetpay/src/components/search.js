import Vue from "vue";
import axios from "axios";
import { debounce } from "lodash";

global.Search = new Vue({
  el: "#header-search",
  data: {
    showModal: null,
    requestUrl: `${wpSite}/wp-json/wp/v2/posts?type[]=announcement&type[]=documentation&type[]=blog&type[]=news&type[]=documentation&type[]=page&type[]=solutions&type[]=modules&type[]=partner&type[]=changelog&search=`,
    query: "",
    results: null,
    showOverlay: null,
    showDropdown: null,
    message: "hello"
  },
  methods: {
    enter: function() {
      document.getElementById("header-search").classList.remove("hidden");

      this.showOverlay = true;
      this.showDropdown = true;

      setTimeout(() => {
        const searchField = document.getElementById("search-field");
        searchField.focus();
        searchField.select();
      }, 500);

      console.log("hi");
    },
    leave: function() {
      this.showOverlay = false;
      console.log(this.showOverlay);
      this.query = "";
      this.results = {};
    },
    updateQuery: debounce(function() {
      this.results = {};
      const query = this.query;
      axios
        .get(`${this.requestUrl}"${this.query}"`)
        .then(function(response) {
          // console.log(Search.results);
          // Search.results = response.data;

          response.data.map(res => {
            if (query == Search.query) {
              if (Search.results[res.type]) {
                const newResults = [...Search.results[res.type], res];
                Vue.set(Search.results, res.type, newResults);
              } else {
                Vue.set(Search.results, res.type, [res]);
              }
            } else {
              return null;
            }
          });

          console.log(Search.results);
        })
        .catch(function(error) {
          console.log(error);
        });
    }, 200)
  }
});
