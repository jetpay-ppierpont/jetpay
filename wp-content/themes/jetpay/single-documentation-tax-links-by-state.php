<?php
getHeader();
while (have_posts()) {
  the_post(); ?>
<div class="category-header">
  <?php getCategoryHeader(); ?>
  <div class="container">
    <div class="spacer-2"></div>
    <a href="/docs" class="back-to-home" style="width: fit-content;"><i class="fas fa-arrow-left"></i>&nbsp;Knowledge
      base home</a>
  </div>
</div>
<div id="normal-content" class="container">
  <div class="spacer-2"></div>
  <h1 class="header">Tax Links By State</h1>
  <p>Click on your state or the state you need tax resource information for, and we’ll get you there!</p>
  <?php getUSMap(); ?>
  <div class="spacer-2"></div>
  <p>NOTE: State agency information will open in a new window or tab.</p>
  <div class="spacer-4"></div>
</div>

<?php

} ?>
<?php
getFooter();