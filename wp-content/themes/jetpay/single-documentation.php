<?php
getHeader();
while (have_posts()) {
  the_post(); ?>
<div class="category-header">
  <?php getCategoryHeader(); ?>
  <div class="container">
    <div class="spacer-2"></div>
    <a href="/docs" class="back-to-home" style="width: fit-content;"><i class="fas fa-arrow-left"></i>&nbsp;Knowledge
      base home</a>
  </div>
</div>
<div id="normal-content" class="container">
  <div class="title">
    <div class="spacer-3"></div>
    <h1 class="title"><?php the_title(); ?></h1>
    <div class="spacer-1"></div>
  </div>
  <?php the_content() ?>
  <div class="spacer-4"></div>
</div>

<?php

} ?>
<?php
getFooter();