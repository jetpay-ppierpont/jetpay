<?php
getHeader(array('color' => 'darkSecondary'));
getBlogHeader();
 ?>
<div>
  <div class="container">
    <div class="spacer-4"></div>
    <h1>Blog</h1>
    <div class="spacer-1"></div>
    <h3>Our latest stories.</h3>
    <hr />
    <h4>Recent blogs:</h4>
    <div class="spacer-1"></div>
    <div class="news-archive__container">
      <?php while (have_posts()) {
  the_post();
  $image = get_field('facebook_sharing_image');
  $author = get_field('author_name');
  $title = get_the_title();
  $content = excerpt(15);
  $date = get_the_date();
  ?>
      <div linkto="<?php the_permalink(); ?>" class="card image vertical">
        <img class="card-img" src="<?php echo "$image" ?>" />
        <div class="card__main-content">
          <h5 class="underline"><?php echo $title; ?></h5>
          <p class="paragraph"><?php echo $content; ?></p>
          <div class="footer-container">
            <hr />
            <p class="paragraph"><?php echo $author; ?></p>
            <p class="overline"><?php echo $date; ?></p>
          </div>

        </div>
      </div>
      <?php

} ?>

      <div class="spacer-1"></div>
      <div class="link-pagination">
        <?php echo paginate_links(); ?>
      </div>
      <div class="spacer-2"></div>
    </div>
  </div>
</div>
<?php
getFooter();