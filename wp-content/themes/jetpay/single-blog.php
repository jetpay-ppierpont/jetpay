<?php
while (have_posts()) {
  the_post();
  $category = get_field('blog_category');
  $product = get_field('related_product');
  $image = get_field('featured_image');
  getHeader(array('color' => 'darkSecondary'));
  ?>
<div class="blog">
  <div class="blog-header" style="background-image: url('<?php echo $image; ?>');">
    <div class="blog-header-container">
      <div class="container">
        <h1 class="underline"><?php the_title(); ?></h1>
        <h4><?php if (get_field('subtitle')) ?></h4>
        <h5><?php echo get_field('author_name'); ?></h5>
        <p class="overline"><?php the_date() ?></p>
      </div>

    </div>
  </div>
  <div class="container">
    <div class="spacer-2"></div>
    <div class="blog-main">
      <div class="blog-main__content">
        <?php the_content() ?>
      </div>
      <div class="blog-main__sidebar">
        <?php
        getSocialWidget();
        getRelatedBlogs($category[0], $product[0], get_the_ID());
        ?>

      </div>
      <div class="form">
        <h3 class="nomargin">Sign up for more from the blog.</h3>
        <div class="spacer-1"></div>
        <p class="nomargin subheading">Get weekly updates and summaries.</p>
        <div class="spacer-1"></div>
        <!--[if lte IE 8]>
<script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2-legacy.js"></script>
<![endif]-->
        <script charset="utf-8" type="text/javascript" src="//js.hsforms.net/forms/v2.js"></script>
        <script>
        hbspt.forms.create({
          portalId: "2004225",
          formId: "7446abb7-c2a4-4f1c-9c3e-7036cbb155d4"
        });
        </script>
      </div>
    </div>
  </div>
</div>
</div>


<?php

} ?>

<?php
getFooter();