<?php
getHeader();
while (have_posts()) {
  the_post();
  ?>
<article class="partner-single">
  <div class="partner-single__header">
    <div class="partner-single__header-logos">
      <div class="our-logo" linkto="/"></div>
      <div class="partner-logo" linkto="<?php echo get_field('url'); ?>"
        style="background-image: url('<?php echo get_field('logo'); ?>');"></div>
    </div>
  </div>
  <div class="spacer-2"></div>
  <div class="breadcrumbs container">
    <a href="/partner" class="breadcrumb">
      <p class="back-to-partner-archive"><i class="fas fa-arrow-left"></i>&nbsp;All Partners</p>
    </a>
  </div>
  <div class="spacer-4"></div>
  <div class="partner-content container">
    <div class="content">
      <h1 class="partner-name"><?php echo get_field('name'); ?></h1>
      <div class="partner-content"><?php echo get_field('description'); ?></div>
    </div>
    <div class="spacer-4"></div>
    <div class="contact">
      <form method="get" action="<?php echo get_field('url'); ?>">
        <button class="filled orange" type="submit">Visit <?php echo get_field('name'); ?>&nbsp;<i
            class="fas fa-arrow-right"></i></button>
      </form>
    </div>
  </div>
  <div class="spacer-4"></div>
</article>
<?php
} ?>

<?php
getFooter();