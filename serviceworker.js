// importScripts('https://storage.googleapis.com/workbox-cdn/releases/3.6.1/workbox-sw.js');
// workbox.routing.registerRoute(
// 	// Cache CSS files
// 	/.*\.js/,
// 	// Use cache but update in the background ASAP
// 	workbox.strategies.staleWhileRevalidate({
// 		// Use a custom cache name
// 		cacheName: 'js-cache',
// 		cacheableResponse: { statuses: [ 0, 200 ] }
// 	})
// );
// workbox.routing.registerRoute(
// 	// Cache CSS files
// 	/.*\.css/,
// 	// Use cache but update in the background ASAP
// 	workbox.strategies.staleWhileRevalidate({
// 		// Use a custom cache name
// 		cacheName: 'css-cache',
// 		cacheableResponse: { statuses: [ 0, 200 ] }
// 	})
// );

// /**
//  * CSS, scripts and images from the assets directory.
//  */
// workbox.routing.registerRoute(/wp-content\/themes\/jetpay\/assets\/.+/, workbox.strategies.staleWhileRevalidate());

// workbox.routing.registerRoute(
// 	// Cache image files
// 	/.*\.(?:png|jpg|jpeg|svg|gif)/,
// 	// Use the cache if it's available
// 	workbox.strategies.staleWhileRevalidate({
// 		// Use a custom cache name
// 		cacheName: 'image-cache',
// 		cacheableResponse: { statuses: [ 0, 200 ] },
// 		plugins: [
// 			new workbox.expiration.Plugin({
// 				// Cache for a maximum of a week
// 				maxAgeSeconds: 7 * 24 * 60 * 60
// 			})
// 		]
// 	})
// );

// // Cache the Google Fonts stylesheets with a stale-while-revalidate strategy.
// workbox.routing.registerRoute(
// 	/^https:\/\/fonts\.googleapis\.com/,
// 	workbox.strategies.staleWhileRevalidate({
// 		cacheName: 'google-fonts-stylesheets'
// 	})
// );

// workbox.routing.registerRoute(
// 	'/',
// 	workbox.strategies.staleWhileRevalidate({
// 		cacheName: 'homepage',
// 		cacheableResponse: { statuses: [ 0, 200 ] },
// 		plugins: [
// 			new workbox.expiration.Plugin({
// 				maxAgeSeconds: 60 * 60 * 24 * 1,
// 				maxEntries: 50
// 			})
// 		]
// 	})
// );

// workbox.precaching.precacheAndRoute(self.__precacheManifest || []);
