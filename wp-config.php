<?php
define('WP_CACHE', false);

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings ** //

if (file_exists(dirname(__FILE__) . '/local.php')) {
	// Local DB settings
	define('DB_NAME', 'local');
	define('DB_NAME', 'local');
	define('DB_USER', 'root');
	define('DB_PASSWORD', 'root');
	define('DB_HOST', 'localhost');
} else {
	// Live DB settings
	define('WP_HOME', 'https://cloud.jetpay.com');
	define('WP_SITEURL', 'https://cloud.jetpay.com');
	define('DB_NAME', 'mktingad_stagingdata');
	define('DB_USER', 'mktingad_pydi3');
	define('DB_PASSWORD', 'wJKq}g{o{Ff{');
	define('DB_HOST', 'localhost');
}

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '&fAB?Zk2MErsS3v+$|`lK-=FIr_M/1^%t5S_tpC;ZC7K9a45wf%c2I-$G|rl2YP ');
define('SECURE_AUTH_KEY', '`u+)!Is dB.lLvS6kh:/n(JK|^)%FZ8[dNQ@`b[`dOnwG.+zaH]C@j@|YL?#Q(M;');
define('LOGGED_IN_KEY', 'r,UkO,dCBj+G>cN]k8rOUk|@tCoN2;YC3{DO5%~xfZz)peBuHr,QT;V^OUT][a75');
define('NONCE_KEY', '&qZ!{sj<z?4()dG|zTo2Z~+2A{b=p9*^fun4s(1>zYf^@x[8:~Ky|L9QF_Cu|s%|');
define('AUTH_SALT', 'l.L(7{Tw]6m+o Rum)k`9e&r>yIu-/NrHe.m%:^|N-i.|sUEtbL--8{-JQ{1U:*~');
define('SECURE_AUTH_SALT', 'Z4k*{`zp0n2HFY&L&Yf@QjC^4m7Zc3@DTp xp:I_zf]hLwI^Qpy X1p;KZ<|me9[');
define('LOGGED_IN_SALT', 'AW-`,Ro)5,330?{A?e%ejBZo!{&4BOV<KN&3kiLz&64k~|lOK_*.7MgZ*<c&&A7H');
define('NONCE_SALT', 'e>[~,pw_y.bv0QH6hZjO-&6lQ+Vw!M,AQ}/thpWZ@DDJn2B1|J>;sm<`90Ua7Z*7');
/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

// increase memory size...
define('WP_MEMORY_LIMIT', '256M');

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if (!defined('ABSPATH'))
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';